//
//  DetailViewController.swift
//  CLASH
//
//  Created by Eduardo Jordan on 18/3/21.
//

import UIKit

class DetailViewController: UIViewController {
    
    @IBOutlet weak var imgClash: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    
    var getName: String = ""
    var getImage: String = ""
    var getDataClash = [Cards]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupImage()
        setupLabels()
        setupView()
        
    }
    
    func setupView() {
        self.view.backgroundColor = Colors.clashBlues
    }
    
    func setupLabels() {
        lblName.text = getName
        lblName.font = UIFont.boldSystemFont(ofSize: 25.0)
        lblName.textColor = Colors.clashYellow
       
    }
    
    func setupImage() {
        let url = URL(string: getImage)
        let data = try? Data(contentsOf: url!)
        imgClash.image = UIImage(data: data!)
    }

}
