//
//  ExtMainViewController.swift
//  CLASH
//
//  Created by Eduardo Jordan on 18/3/21.
//

import Foundation
import UIKit

extension MainViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayCards.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        
        cell.textLabel?.text = arrayCards[indexPath.row].name
        cell.textLabel?.textColor = Colors.clashWhite
        cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 20.0)
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
        
        tableView.backgroundColor = Colors.clashBlues
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let storybaord = UIStoryboard(name: "Main", bundle: nil)
        let detailVC = storybaord.instantiateViewController(withIdentifier: "DetailViewController") as? DetailViewController
        detailVC!.modalPresentationStyle = .fullScreen
        detailVC!.getName = arrayCards[indexPath.row].name
        detailVC!.getImage = arrayCards[indexPath.row].iconUrls.medium
        
        navigationController?.pushViewController(detailVC!, animated: true)
        
    }
    
}
