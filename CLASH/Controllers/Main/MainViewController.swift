//
//  MainViewController.swift
//  CLASH
//
//  Created by Eduardo Jordan on 18/3/21.
//

import UIKit
import Foundation

class MainViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityindicator: UIActivityIndicatorView!
    
    var arrayCards = [Item]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getData()
        setupView()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupNavBar()
    }
    
    func setupView() {
        self.view.backgroundColor = Colors.clashBlues
    }
    
    func setupNavBar() {
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 60, height: 35))
        imageView.contentMode = .scaleAspectFit
        let image = UIImage(named: "clashLogo")
        imageView.image = image
        navigationItem.titleView = imageView
    }
    
    func getData() {
        activityindicator.isHidden = false
        activityindicator.startAnimating()
         Request.shared.getRequestData(endpoint: "", completion: { (result) in
            self.arrayCards.removeAll()
            for item in result.items {
                self.arrayCards.append(item)
                self.arrayCards  =  self.arrayCards.sorted {
                    $0.name < $1.name
                }
            }
         
            DispatchQueue.main.async { [self] in
                activityindicator.isHidden = true
                activityindicator.stopAnimating()
                tableView.reloadData()
            }
        })
    }

}
