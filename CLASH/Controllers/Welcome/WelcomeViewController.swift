//
//  WelcomeViewController.swift
//  CLASH
//
//  Created by Eduardo Jordan on 17/3/21.
//

import UIKit
import Alamofire

class WelcomeViewController: UIViewController {
    
    @IBOutlet weak var imgWelcome: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var btnInitial: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLabels()
        setupButtons()
        setupImage()
        setupView()
    }
    
    func setupView() {
        self.view.backgroundColor = Colors.clashBlues
    }
    
    func setupLabels() {
        lblName.text = localizedString("text_name")
        lblDate.text = localizedString("text_date")
        lblEmail.text = localizedString("text_email")
        
        lblName.font = UIFont.boldSystemFont(ofSize: 20.0)
        lblDate.font = UIFont.systemFont(ofSize: 16.0)
        lblEmail.font = UIFont.boldSystemFont(ofSize: 16.0)
        
        lblName.textColor = Colors.clashWhite
        lblDate.textColor = Colors.clashWhite
        lblEmail.textColor = Colors.clashWhite
    }
    
    func setupButtons() {
        btnInitial.layer.cornerRadius = 20
        btnInitial.setTitle(localizedString("text_start"), for: .normal)
        btnInitial.addTarget(self, action: #selector(buttonAnimation(_:)), for: .touchUpInside)
        
    }
    
    func setupImage() {
        imgWelcome.image = UIImage(named: "welcomeImage")
        
    }
    
    @objc func buttonAnimation(_ sender: UIButton) {
 // Este efecto se desactivo por que no se aprecia en la app
//        let pulse = PulseAnimation(numberOfPulse: 1, radius: 200, postion: sender.center)
//        pulse.animationDuration = 1.0
//        pulse.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
//        self.view.layer.insertSublayer(pulse, below: self.view.layer)
        
    }
    
}
