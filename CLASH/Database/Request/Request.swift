//
//  Request.swift
//  CLASH
//
//  Created by Eduardo Jordan on 17/3/21.
//

import Foundation
import Alamofire
import UIKit

class Request: NSObject {
    
    static let shared = Request()
    
    fileprivate var baseUrl =  "https://api.clashroyale.com/v1/cards" //"https://restcountries.eu/rest/v2"
    fileprivate var token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiIsImtpZCI6IjI4YTMxOGY3LTAwMDAtYTFlYi03ZmExLTJjNzQzM2M2Y2NhNSJ9.eyJpc3MiOiJzdXBlcmNlbGwiLCJhdWQiOiJzdXBlcmNlbGw6Z2FtZWFwaSIsImp0aSI6IjIwYjUxZmU3LTc4NDEtNGYxNS05M2VlLTJhZGI1YWFiNDg1YSIsImlhdCI6MTYxNjA2ODQzMiwic3ViIjoiZGV2ZWxvcGVyLzIwNzBiMTE0LWE0ZTgtYWU0Ny1kYzI3LTNlZTAzNDYwNWY1MCIsInNjb3BlcyI6WyJyb3lhbGUiXSwibGltaXRzIjpbeyJ0aWVyIjoiZGV2ZWxvcGVyL3NpbHZlciIsInR5cGUiOiJ0aHJvdHRsaW5nIn0seyJjaWRycyI6WyI4NS41OS43Ny45MCJdLCJ0eXBlIjoiY2xpZW50In1dfQ.iwg_5oEDWNB8xiCBlMO0wfznXep-NLgKMvyp6NCw1165z61niucKnOnQ_D6USWClk8LF-PLkxsW5kN6Gis_Jmw"
    
    func getRequestData(endpoint: String, completion: @escaping (Cards) -> Void) {
        let headersToken: HTTPHeaders = [
            "Authorization": "Bearer \(token)",
            "Content-Type": "application/X-Access-Token"
        ]
        
        AF.request(self.baseUrl + endpoint, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headersToken, interceptor: nil).response { response in
            if response.response?.statusCode == 200 {
                guard let data = response.data else {return}
                do {
                    let cards = try JSONDecoder().decode(Cards.self, from: data)
                    completion(cards)
                } catch let error {
                    print(error.localizedDescription)
                }
            } else {
                print("error")
            }
            
        }
    }
    // ACA REALIZAR LAS OTRAS LLAMADAS "postRequestData" o "putRequestData"
    
}
