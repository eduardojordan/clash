//
//  Cards.swift
//  CLASH
//
//  Created by Eduardo Jordan on 17/3/21.
//

import Foundation

struct Cards: Decodable {
    let items: [Item]
}

struct Item: Decodable {
    let name: String
    let id: Int
    let maxLevel: Int
    let iconUrls: IconUrls
}

struct IconUrls: Decodable {
    let medium: String
}
