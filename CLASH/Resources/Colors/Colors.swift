//
//  Colors.swift
//  CLASH
//
//  Created by Eduardo Jordan on 17/3/21.
//

import UIKit

struct Colors {
    
    static var clashBlues: UIColor = UIColor.init(hexString: "#0B89FF")!
    static var clashWhite: UIColor = UIColor.init(hexString: "#FFFFFF")!
    static var clashYellow: UIColor = UIColor.init(hexString: "#F9CD5F")!
    
}


