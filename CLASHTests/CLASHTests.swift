//
//  CLASHTests.swift
//  CLASHTests
//
//  Created by Eduardo Jordan on 17/3/21.
//

import XCTest
@testable import CLASH

class CLASHTests: XCTestCase {

    func testParseJsonFile() throws {
        guard let pathString = Bundle(for: type(of: self)).path(forResource: "clashRoyale", ofType: "json") else {
            fatalError("json not found")
        }
        guard let json = try? String(contentsOfFile: pathString, encoding: .utf8) else {
            fatalError("Unable to convert jsont to String")
        }
        let jsonData = json.data(using: .utf8)!
        let card = try JSONDecoder().decode(Cards.self, from: jsonData)

        XCTAssertEqual("Knight", card.items[0].name)
       
    }

}
