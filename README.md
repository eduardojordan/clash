![](images/Untitled-3.png)

![IMAGE_DESCRIPTION](https://gitlab.com/eduardojordan/clash/-/raw/master/Untitled-3.png)

## Realizado en:
 Xcode 12.2
Swift 5

##Version de iOS 
14.2

##Cocoapods
Alamofire

## APIREST
https://developer.clashroyale.com/#/

##Decisiones sobre el desarrollo


Observaciön: Se trabaja con la API oficial del Clash Royale tratando de seguir los mismo lineamientos planteados para la practica, debido a que la API suministrada  se encontraba en mantenimiento.

- Se incluyo una librería ( ALAMOFIRE) para cumplir con los requerimientos tambien se puede manejar la llamada de Datos con URLSession si se quiere evitar esta librería
- El modelo de datos se implemento con Codable
- Se uso cocoapods para incluir la librería anteriormente descrita
- Esta diseñada para todos los dispositivos iPhone
- Se incluye un test Unitario del json
- El listado se ordenado alfabeticamente
- Se localizacion de idiomas
- Se incluye manejo de errores


La app se creo con una arquitectura MVC  por las caracterizas del proyecto me pareció lo más practico y rápido. Se incluyen varias carpetas para manejar por separado la llamada de datos , el color , Idiomas,  Imágenes,  etc , con la idea de hacer mas estructurado el manejo de esta información.
Se incluye  gitIgnore y Swiflint para mantener un código limpio durante el desarrollo. Se uso el storyboard y código en algunas ocasiones.
Se realizo utilizando Sourcetree para realizar commits al repositorio
